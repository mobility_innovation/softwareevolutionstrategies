# README #

The integer programming model modelled in this code can be used to plan holistic software evolution strategies for connected and intelligent vehicles.
The associated methodology is described in detail in Block et al. (2021) "Designing Software Evolution Strategies for Connected and Intelligent Vehicles" (currently under review for the IEEE IV conference).
Please also refer to the cited publication to better understand the usage of the code and the meaning of the variables.

The repository is strucutred as follows:

* The package de.fraunhofer.iao.softwareEvolution is a python package with all the necessary classes and functions to setup the integer programming problem
* main.py file contains the glue and specification code for the above cited paper
* The folder results contains the planned software evolution strategy for the use case, described in the paper per step of the methdology. The first spreadsheet furthermore displays the sorted list of priorities and the rational behind the constraint desicions.
 
### How do I get set up? ###

The code is written in Python for Python3.6 and only has few dependencies. Most of them can be installed via pip.

* [Numpy](https://numpy.org/) for arithemtic operations
* [NetworkX](https://networkx.org/) for graph and dependency processing
* [Google OR Tools](https://developers.google.com/optimization/introduction/python) to solve the integer programming problem

### Who do I talk to? ###

* If you have any questions regarding the paper or the code, please contact the authors of the cited paper
* If you reuse the code in your scientific work, please cite the above mentioned paper, as soon as it is officially published
* For licensing questions please refer to the LICENSE file (MIT License)