'''
Created on 13.01.2021

@author: Lukas Block
'''
import networkx as nx

from de.fraunhofer.iao.softwareEvolution.PartialComponentState import PartialComponentState

class Component(object):
    '''
    This class represents a software component, which can be cyclically altered.
    It can be in multiple different states (see Partial states). Superstate, incompatible
    partial states and compatible partial states are defined via this class, to form
    one consistent state of this software component over time.
    '''

    # Some helper variables
    INCOMPATIBLE = 'incompatible'
    SUPERSTATE = 'superstate'
    CAN_BE_MADE_COMPATIBLE = 'can_be_made_compatible'
    
    
    def __init__(self, name, start_cycle, cycle_time, no_time_frames):
        '''
        Constructor
        @param name: The name of this software component
        @param start_cycle: The initial offset of the release cycle
        @param cycle_time: The duration of the release cycle in number of time
        frames
        @param no_time_frames: The maximum number of time frames for this model
        (planning horizon)
        '''
        assert isinstance(name, str)
        self._name = name
        
        assert start_cycle < cycle_time
        assert isinstance(start_cycle, int)
        self._start_cycle = start_cycle
        
        assert isinstance(cycle_time, int)
        self._cycle_time = cycle_time
        
        assert isinstance(no_time_frames, int)
        self._no_time_frames = no_time_frames
        
        self._partial_states = nx.DiGraph()
        self._initial_states = set()
        
    @property
    def name(self):
        return self._name
    
    @property
    def no_time_frames(self):
        return self._no_time_frames
    
    @property
    def cycle_time(self):
        return self._cycle_time
    
    @property
    def start_cycle(self):
        return self._start_cycle
    
    @property
    def partial_states(self):
        return set(self._partial_states.nodes)
    
    def partial_state_by_name(self, name):
        '''
        Returns a partial state by name
        '''
        for ps in self.partial_states:
            if ps.name == name:
                return ps
        return False
    
    @property
    def state_relations(self):
        return set(self._partial_states.edges)
    
    @property
    def incompatibilities(self):
        return set([(x[0], x[1]) for x in self._partial_states.edges.data('cmpt') if (x[2] == Component.INCOMPATIBLE)])
    
    @property
    def substates(self):
        return set([(x[0], x[1]) for x in self._partial_states.edges.data('cmpt') if (x[2] == Component.SUPERSTATE)])
    
    @property
    def can_be_made_compatible_states(self):
        return set([(x[0], x[1]) for x in self._partial_states.edges.data('cmpt') if (x[2] == Component.CAN_BE_MADE_COMPATIBLE)])
    
    def add_partial_state(self, name, available=0):
        '''
        Creates a partial state for this component.
        @param name: The name of this partial state
        @param avaiable: Optional parameter, indicating when this partial state
        is available for implementation (as a time frame number)
        '''
        result = PartialComponentState(name, self, available)
        self._partial_states.add_node(result)
        return result
    
    def set_initial_state(self, ps):
        assert ps in self.partial_states
        self._initial_states.add(ps)
        
    def add_initial_state(self, name, available=0):
        '''
        Creates a partial state for this component and sets it as the initial
        state at time frame 0.
        '''
        new_state = self.add_partial_state(name, available)
        self.set_initial_state(new_state)
        return new_state
        
    @property
    def initial_states(self):
        return self._initial_states
    
    def set_incompatibe(self, p1, p2):
        '''
        Sets the compatibility state incompatible
        '''
        assert p1 in self._partial_states.nodes
        assert p2 in self._partial_states.nodes
        # Remove all existing edges
        if self._partial_states.has_edge(p1, p2):
            self._partial_states.remove_edge(p1, p2)
        if self._partial_states.has_edge(p1, p2):
            self._partial_states.remove_edge(p1, p2)
        # Set the new edges - incompatibility is a two way relationship
        self._partial_states.add_edge(p1, p2, cmpt=Component.INCOMPATIBLE)
        self._partial_states.add_edge(p2, p1, cmpt=Component.INCOMPATIBLE)
        
    def set_substate(self, subState, superState):
        '''
        Sets the compatbility relation substate - superstate.
        '''
        assert subState in self._partial_states.nodes
        assert superState in self._partial_states.nodes
        # Remove the previously existing edge
        if self._partial_states.has_edge(subState, superState):
            self._partial_states.remove_edge(subState, superState)
        # Remove the edge the other way around, if it is an incompatible edge
        if self._partial_states.has_edge(superState, subState) and (self._partial_states.get_edge_data(superState, subState)['cmpt'] == Component.INCOMPATIBLE):
            self._partial_states.remove_edge(superState, subState)
            
        # Set the new edge pointing from the substate to the superstate
        self._partial_states.add_edge(subState, superState, cmpt=Component.SUPERSTATE)
        
    def set_can_be_made_compatible(self, a, b, ease):
        '''
        Sets the compatibility relation can be made compatible
        '''
        assert a in self._partial_states.nodes
        assert b in self._partial_states.nodes
        assert isinstance(ease, int) or isinstance(ease, float)
        '''
        Bidirectional relationship - a can be made compatible to b, they can coexist
        '''
        # Remove the previously existing edge
        if self._partial_states.has_edge(a, b):
            self._partial_states.remove_edge(a, b)
        # Remove the edge the other way around
        if self._partial_states.has_edge(b, a):
            self._partial_states.remove_edge(b, a)
            
        # Now assign the compatibility edge
        self._partial_states.add_edge(a, b, cmpt=Component.CAN_BE_MADE_COMPATIBLE, ease=ease)
        self._partial_states.add_edge(b, a, cmpt=Component.CAN_BE_MADE_COMPATIBLE, ease=ease)
        
    def generate_model_variables(self, model):
        '''
        Function to generate the model variables for the integer programming problem
        '''
        for ps in self._partial_states:
            ps.generate_model_variables(model)
    
    def add_constraints(self, model):
        '''
        Set the incompatibility constraints
        '''
        for ic in self.incompatibilities:
            for t in range(self.no_time_frames):
                # Get the first ps and the second ps
                ps1 = ic[0].model_variable_for_time(t)
                ps2 = ic[1].model_variable_for_time(t)
                # It is not an inequality, but rather that both cannot be true at the same time
                model.Add(ps1 == 0).OnlyEnforceIf(ps2)
                model.Add(ps2 == 0).OnlyEnforceIf(ps1)
                
        '''
        Set the substate constraints
        '''
        for ic in self.substates:
            for t in range(self.no_time_frames):
                ps1 = ic[0].model_variable_for_time(t)
                ps2 = ic[1].model_variable_for_time(t)
                model.Add(ps1 >= ps2)
        
        '''
        Set the constraint that changes in the partial state's state can only
        happen within the allowed cycle times.
        Enforces the update cycles, i.e. if it has not been updated in one cycle,
        it can only be updated in the next cycle and not in between
        '''
        # Apply the constraints to all partial states
        for ps in self.partial_states:
            prev_node = None
            for t in range(self._no_time_frames):
                # If the ref variable is None, set it to the current time frame
                if prev_node is None:
                    prev_node = ps.model_variable_for_time(t)
                else:
                    # We are in a dependency
                    model.Add(prev_node == ps.model_variable_for_time(t))
                    
                # Now set the previous variable again to null, if there is a break, between this t and the next one
                if ((t+1)%self._cycle_time) == self._start_cycle:
                    prev_node = None
 
        '''
        Set constraints to the initial partial states of this component, that
        they must be true, and to the remaining constraints, that they must be false
        '''
        # Firstly we need to check, that this component cannot be altered in the
        # first step, because then the initial state can directly be changed
        if self._start_cycle > 0:
            for ps in self.partial_states:
                if ps in self._initial_states:
                    # If it is an initial state it must be active in the first round
                    model.Add(ps.model_variable_for_time(0) == 1)
                else:
                    # If it is not an initial state it is not allowed to be active
                    model.Add(ps.model_variable_for_time(0) == 0)
 
    def get_few_changes_objective(self, model):
        '''
        Sets up the submodel for the integer programming problem, representing
        the objective function, that we prefer a solution with as few changes as
        possible.
        '''
        result = []
        
        # This function delivers variables which denote how often a component does change
        for t in range(self.no_time_frames-1):
            # Single vars of the partial states, indicatinv whether something changed
            single_vars = [ps.changed(t, model) for ps in self.partial_states]
            
            # Name of the objective variable for this state, set to true, as soon as one state changed
            objective_t_var = model.NewBoolVar(self.name + "#objective#" + str(t))
            model.AddMaxEquality(objective_t_var, single_vars)
            result.append(objective_t_var)
            
        # Now generate a variable that equals the sum of the all changes
        objective_name = self.name + "#objective"
        objective_var = model.NewIntVar(0, self.no_time_frames-1, objective_name)
        model.Add(objective_var == sum(result))
        return objective_var
            
            
            
            