'''
Created on 13.01.2021

@author: Lukas Block
'''

def variables_in_model(model):
    '''
    Returns all variables by name in the integer programming problem
    '''
    return set([v.name for v in model.Proto().variables])

def print_comps_result(no_time_frames, comps, solver, exclude=None):
    '''
    Helper function for printing
    '''
    # Instantiate the list to print
    print_list = []
    
    # Make a component list to preserve order
    comps_list = list(comps.values())
    # Got through each comp
    for c in comps_list:
        c_print_list = [[] for _ in range(no_time_frames)]
        
        for ps in c.partial_states:
            for t in range(no_time_frames):
                v = ps.model_variable_for_time(t)
                # Add it to the print list, if the variable is set
                if int(solver.Value(v)) == 1:
                    # Check if we should exclude this result from being printed
                    if exclude is not None:
                        if exclude(ps.name, t):
                            continue
                    c_print_list[t].append(ps.name)
        # Append the print list for this component to the total print list
        print_list.append(c_print_list)
    # Now print the print list
    print_multi_time_array(no_time_frames, [c.name for c in comps_list], print_list)
            
def print_multi_time_array(no_time_frames, keys, values):
    '''
    Helper function for printing
    '''
    for c_name, c_part in zip(keys, values):
        print(c_name)
        # Variable to keep track of the number of prallel states in the print
        run_loop = True
        i = 0
        while (run_loop):
            line_to_print = ""
            run_loop = False
            for t in range(no_time_frames):
                if len(c_part[t]) > i:
                    # If there is still a variable to print, do it
                    line_to_print += c_part[t][i]
                    run_loop = True
                else:
                    # If there is no variable to print, add whitspace as a placeholder
                    line_to_print += ""
                # Add two tabs as placeholders between two timeframes
                line_to_print += "\t"
            # Now print the line
            print(line_to_print)
            i += 1
    
def print_system_state_result(no_time_frames, system_states, solver):
    '''
    Helper function for printing
    '''
    # Instantiate the list to print
    print_list = [[] for _ in range(no_time_frames)]
    
    # Make a component list to preserve order
    # Got through each comp
    for ss in system_states:
        for t in range(no_time_frames):
            v = ss.model_variable_for_time(t)
            # Add it to the print list, if the variable is set
            if int(solver.Value(v)) == 1:
                print_list[t].append(ss.name)
    # Now print the print list
    print_multi_time_array(no_time_frames, ["system states"], [print_list])
