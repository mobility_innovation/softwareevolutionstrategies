'''
Created on 13.01.2021

@author: Lukas Block
'''

import de.fraunhofer.iao.softwareEvolution.Component

from de.fraunhofer.iao.softwareEvolution.PartialState import PartialState

class PartialComponentState(PartialState):
    '''
    Represents a partial component state of a component. A partial state defines
    partial requirements towards a software component.
    '''

    def __init__(self, name, component, available=0):
        '''
        Constructor
        @param name: The name of this partial state
        @param component: The parent component of this partial state
        @param availabe: Optional parameter, describing when this partial state
        is available for implementation
        '''
        assert isinstance(component, de.fraunhofer.iao.softwareEvolution.Component.Component)
        self._component = component

        super().__init__(name, component.no_time_frames)
        
        # The setter will take care of all the checking
        self.available = available
        
        
    @property
    def component(self):
        return self._component
    
    @property
    def id(self):
        return self._component.name + "#" + self.name
    
    @property
    def available(self):
        return self._available
    
    @available.setter
    def available(self, available):
        assert isinstance(available, int)
        assert 0 <= available < self.no_time_frames
        self._available = int(available)
        
    def add_constraints(self, model):
        # Set all time frames to zero, which were before the available date
        for t in [x for x in range(self.component.no_time_frames) if x < self.available]:
            model.Add(self.model_variable_for_time(t) == 0)
    
    def changed(self, t, model):
        '''
        This function must return a model variable, indicating whether the partial state
        changed from time t to time t+1
        '''
        assert 0 <= t < (len(self._model_variables)-1)
        helper_name = self.id + "#chelper#" + str(t)
        changed_name = self.id + "#changed#" + str(t)
        
        # We need to implement an XOR between to consequitve states of partial
        # states. This is done, by substracting the two variables from
        # each other and assigning abs() to it
        # The helper is necessary to calculate the difference between two variables
        helper_var = model.NewIntVar(-1, 1, helper_name)
        model.Add((self.model_variable_for_time(t) - self.model_variable_for_time(t+1) == helper_var))
        
        # Now make it to an abs
        changed_var = model.NewBoolVar(changed_name)
        model.AddAbsEquality(changed_var, helper_var)
        
        return changed_var