'''
Created on 17.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.PartialLogicalState import PartialLogicalState

class PartialLogicalAndState(PartialLogicalState):
    '''
    Partial State of something, is only active if the conditions are all active.
    '''


    def __init__(self, name, conditions, no_time_frames):
        '''
        Constructor - see superclass
        '''
        super().__init__(name, conditions, no_time_frames)
        
    @property
    def id(self):
        return self.name + '#and' + super().id
            
    def add_constraintes_per_time_frame(self, t, own_variable, constraints, model):
        model.AddMinEquality(own_variable, constraints)
        
        