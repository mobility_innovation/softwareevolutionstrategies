'''
Created on 17.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.PartialLogicalState import PartialLogicalState

class PartialLogicalOrState(PartialLogicalState):
    '''
    Partial State of something, is only active if one of the conditions is active.
    '''


    def __init__(self, name, conditions, no_time_frames):
        '''
        Constructor
        '''
        super().__init__(name, conditions, no_time_frames)
        
    @property
    def id(self):
        return self.name + '#or' + super().id
        
    def add_constraintes_per_time_frame(self, t, own_variable, constraints, model):
        model.AddMaxEquality(own_variable, constraints)
        
        