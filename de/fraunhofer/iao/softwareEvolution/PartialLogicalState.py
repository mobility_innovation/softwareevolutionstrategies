'''
Created on 17.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.PartialState import PartialState

from collections.abc import Iterable

class PartialLogicalState(PartialState):
    '''
    A Partial Logical state of something (component or system), which is conditioned on
    some other partial states.
    '''


    def __init__(self, name, conditions, no_time_frames):
        '''
        Constructor
        @param name: Override
        @param conditions: An iterable containing other partial states, on which
        this state depends.
        @param no_time_frames: Override 
        '''
        super().__init__(name, no_time_frames)
        
        assert isinstance(conditions, Iterable)
        for c in conditions:
            assert isinstance(c, PartialState)
        self._conditions = set(conditions)
        
    @property
    def id(self):
        result = '('
        for c in self._conditions:
            result += c.id + ", "
        result += ')'
        return result
        
    @property
    def conditions(self):
        return self._conditions
    
    def add_condition(self, c):
        assert isinstance(c, PartialState)
        self._conditions.add(c)
        
    def remove_condition(self, c):
        self._conditions.remove(c)
        
    def generate_model_variables(self, model):
        # Add the model variables
        super().generate_model_variables(model)
        
        # For all conditions, add the variables as well
        for c in self._conditions:
            c.generate_model_variables(model)
    
    def add_constraints(self, model):
        # First generate the own constraints
        for t in range(self.no_time_frames):
            cs = set([ps.model_variable_for_time(t) for ps in self._conditions])
            self.add_constraintes_per_time_frame(t, self.model_variable_for_time(t), cs, model)
            
        # Afterwards, invoke all constraints of the conditions
        for c in self._conditions:
            c.add_constraints(model)
        
        super().add_constraints(model)
        
    def add_constraintes_per_time_frame(self, t, own_variable, constraints, model):
        '''
        Function to implement cretain constraints which are applicable per each time frame
        '''
        # To implement in subclasses
        pass
        