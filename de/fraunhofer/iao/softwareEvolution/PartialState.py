'''
Created on 13.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.Helpers import variables_in_model


class PartialState(object):
    '''
    A partial state describes a part of a fully defined state of a software component
    or the system as a whole. It might be the fully defined state but it is not
    necessarily. Each partial state maintains a boolean value for each time
    frame, describing whether the requirements for this partial state are fulfilled
    and thus, whether the parital state is active.
    This is a superclass for most of the relevant classes in this framework.
    '''

    def __init__(self, name, no_time_frames):
        '''
        Constructor
        @param name: Human readable name of this partial state
        @param no_time_frames: The maximum number of time frames for this model
        '''
        assert isinstance(name, str)
        self._name = name
        
        assert isinstance(no_time_frames, int)
        self._no_time_frames = no_time_frames
        
        # The model variables of the linear problem
        self._model_variables = []
    
    @property
    def name(self):
        return self._name
    
    @property
    def id(self):
        '''
        Unique id of this partial state, relevant for the integer programming problem
        '''
        return self.name
    
    def time_id(self, t):
        '''
        Each partial state maintains a boolean value foreach timeframe. This is
        the id for each of these boolean variables.
        '''
        return self.id + "#t" + str(t)
    
    @property
    def model_variables(self):
        '''
        The model variables of this partial state, one for each time frame
        '''
        assert len(self._model_variables) > 0
        return self._model_variables
        
    def model_variable_for_time(self, t):
        '''
        Returns the model variables for a certain time frame t
        '''
        assert len(self._model_variables) > t
        return self._model_variables[t]
    
    @property
    def no_time_frames(self):
        return self._no_time_frames
    
    def generate_model_variables(self, model):
        '''
        Function called from external to generate the Google OR Tools variables
        for this partial state
        '''
        # Check if this assignment was already called before
        if len(self._model_variables) == 0:
            for t in range(self._no_time_frames):
                # Check if the variable has already been assigned
                # It is only allowed to have a variable with the same, if it is
                # a variable of this partial state
                if self.time_id(t) not in variables_in_model(model):
                    self._model_variables.append(model.NewBoolVar(self.time_id(t)))
                else:
                    # We have duplicated variables
                    assert False
            # Do nothing in this else part, because this is regular behavior,
            # we want to prevent double initialization
                
            
    def add_constraints(self, model):
        '''
        Function called from external to generate the constraints for the Google
        OR Tools model
        '''
        # To be implemented in subclasses
        pass
        
    def __eq__(self, other):
        return self.id == other.id
    
    def __hash__(self):
        return hash(self.id)
        