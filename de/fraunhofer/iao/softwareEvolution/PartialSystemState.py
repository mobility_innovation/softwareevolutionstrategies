'''
Created on 14.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.PartialLogicalOrState import PartialLogicalOrState

class PartialSystemState(PartialLogicalOrState):
    '''
    This class represents a partial state of the system as a whole. Such a partial
    state is used to describe the required state of the system after a change, i.e.
    it is similar to a change request. It is by default a logical or state,
    because normally we have multiple possibilities, how to create the solution
    for a specific change request.
    Thus, the conditions are the different solutions for this change request.
    '''

    def __init__(self, name, earliest, latest, until, conditions, no_time_frames):
        '''
        Constructor
        @param name: Override
        @param earliest: The earliest time frame, when this partial system state
        can be implemented
        @param latest: The latest time frame, when this partial system state needs
        to be implemented
        @param until: The time frame until which the partial state must be active,
        equivalent to t_upper in the paper
        @param conditions: Override
        @param no_time_frames: Override
        '''
        super().__init__(name, conditions, no_time_frames)
        
        assert isinstance(latest, int)
        assert isinstance(until, int)
        assert isinstance(earliest, int)
        assert earliest <= latest < until
        self._latest = latest
        self._until = until
        self._earliest = earliest
        
    @property
    def latest(self):
        return self._latest
    
    @property
    def until(self):
        return self._until
    
    @property
    def earliest(self):
        return self._earliest
          
    def add_constraintes_per_time_frame(self, t, own_variable, constraints, model):
        super().add_constraintes_per_time_frame(t, own_variable, constraints, model)
        
        # Constraint: Own variable must be set to false, if the time frame is before earliest
        if t < self.earliest:
            model.Add(own_variable == 0)
        
        # Constraint: Own variable must be set to true, if the time frame is in between latest and until
        if self.latest <= t < self.until:
            model.Add(own_variable == 1)
        
        # Once set, the partial state must remain true, until the until date
        if 0 < t < self.until:
            model.Add(own_variable >= self.model_variable_for_time(t-1))
                
                
                
                
                
                
                