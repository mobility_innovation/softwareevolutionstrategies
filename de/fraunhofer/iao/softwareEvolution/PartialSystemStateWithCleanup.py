'''
Created on 17.01.2021

@author: Lukas Block
'''
from de.fraunhofer.iao.softwareEvolution.PartialSystemState import PartialSystemState
from de.fraunhofer.iao.softwareEvolution.PartialState import PartialState

class PartialSystemStateWithCleanup(PartialSystemState):
    '''
    This is a partial system state, however it allows to define a certain cleanup
    state, which is the ideal partial system state without architectural technical debt.
    The cleanup state is thus equivalent to s* in the paper.
    '''


    def __init__(self, name, earliest, latest, cleanup_deadline, until, conditions, cleanup_condition, no_time_frames):
        '''
        Constructor
        @param name: Override
        @param earliest: Override
        @param latest: Override
        @param cleanup_deadline: The deadline until when s* must be implemente
        @param until: Override
        @param conditions: Override
        @param cleanup_condition: The ideal partial system state without architecturl
        technical debt, i.e. s*
        @param no_time_frames: Override 
        '''
        super().__init__(name, earliest, latest, until, conditions, no_time_frames)
        
        assert isinstance(cleanup_deadline, int)
        assert latest <= cleanup_deadline < until
        self._cleanup_deadline = cleanup_deadline
        
        assert isinstance(cleanup_condition, PartialState)
        # Make sure, that this condition also leads to this system state
        # Only way to make sure, is to add it to the conditions
        assert cleanup_condition in conditions
        self._cleanup_condition = cleanup_condition
        
    @property
    def cleanup_deadline(self):
        return self._cleanup_deadline
    
    @property
    def cleanup_condition(self):
        return self._cleanup_condition
    
    def add_constraintes_per_time_frame(self, t, own_variable, constraints, model):
        super().add_constraintes_per_time_frame(t, own_variable, constraints, model)
        
        # Now make sure that the cleanup condition is set in between the deadline and until
        if self._cleanup_deadline <= t < self.until:
            model.Add(self.cleanup_condition.model_variable_for_time(t) == 1)
        