'''
Created on 13.01.2021

@author: Lukas Block
'''

from ortools.sat.python import cp_model

from de.fraunhofer.iao.softwareEvolution.Component import Component
from de.fraunhofer.iao.softwareEvolution.PartialSystemState import PartialSystemState
from de.fraunhofer.iao.softwareEvolution.Helpers import print_comps_result,\
    print_system_state_result
from de.fraunhofer.iao.softwareEvolution.PartialLogicalAndState import PartialLogicalAndState
from de.fraunhofer.iao.softwareEvolution.PartialSystemStateWithCleanup import PartialSystemStateWithCleanup
from de.fraunhofer.iao.softwareEvolution.PartialLogicalOrState import PartialLogicalOrState


'''
Firstly setup all constants for the model
'''

# The number of timeframes, we look at in this model
NO_TIME_FRAMES = 36

# Human readable names for the software components
LIDAR = 'lidar'
CAM = 'camera'
RADAR = 'radar'
US = 'ultrasonic'
FUSION = 'fusion'
PLAN = 'planning'
COMP_COMP = 'compatibility_comp'

# Clustering of the components in basic components and components for compatbility
BASE_COMPS = {LIDAR, CAM, RADAR, US, FUSION, PLAN}
COMP_COMPS = {COMP_COMP}
ALL_COMPS = set(BASE_COMPS)
ALL_COMPS.update(COMP_COMPS)

'''
Lidar can be updated instantly in a two week cycle
CAM is due in two weeks and can be adjusted every four weeks
RADAR is due now and can be adjusted every three weeks
Ultrasonic is due in four weeks and can be adjusted every eight weeks
Fusion and planning can always be adjusted / in two week cycles
Compatibility software components can be introduced at any time 
'''
next_update_cycle = {
    LIDAR: [0,2],
    CAM: [2,4],
    RADAR: [0,3],
    US: [4,8],
    FUSION: [0,1],
    PLAN: [0,2],
    COMP_COMP: [0,1]
}

# Human readable names for the different states
IS = 'is'
S1 = 'v1'
S2 = 'v2'
S3 = 'v3'
S4 = 'v4'

'''
!!!!!!!!!!!!!!!!!!!!!
The time constraints for the different change requests, the methodology alters this part
!!!!!!!!!!!!!!!!!!!!!
'''
SS_TIMES = {
    S1: [0, 0, 7, NO_TIME_FRAMES],
    S2: [1, 1, 7, NO_TIME_FRAMES],
    S3: [7, 7, 7, NO_TIME_FRAMES],
    S4: [25, 25, 28, NO_TIME_FRAMES]
}
# The timestamp when the changes are availbe / known
SS_TIMES_AVAILABLE = {
    S1: 0,
    S2: 1,
    S3: 2,
    S4: 25
}

'''
Definition of the partial component states
'''
W_NESTED = "w_nested"
WO_NESTED = "wo_nested"
W_BB = "w_bb"
WO_BB = "wo_bb"
W_UNIT_CHANGE = "w_unit_change"
WO_UNIT_CHANGE = "wo_unit_change"
W_CS_VARIABLE = "w_cs_var"
WO_CS_VARIABLE = "wo_cs_var"
BASE_COMP_STATES = {(W_NESTED, WO_NESTED), (W_BB, WO_BB), (W_UNIT_CHANGE, WO_UNIT_CHANGE), (W_CS_VARIABLE, WO_CS_VARIABLE)}

COMP_COMP_STATES = {(W_NESTED, WO_NESTED), (W_BB, WO_BB), (W_UNIT_CHANGE, WO_UNIT_CHANGE), (W_CS_VARIABLE, WO_CS_VARIABLE)}

# Setting the initial states
INITIAL_STATES = [WO_NESTED, WO_BB, WO_UNIT_CHANGE, WO_CS_VARIABLE]

# Setting up, which partial component states are required by wich change request
SYSTEM_STATE_PARTIAL_STATE_MATCH = {
    IS: [],
    S1: [W_NESTED],
    S2: [W_BB],
    S3: [W_UNIT_CHANGE],
    S4: [W_CS_VARIABLE],
    }

def customize(comps):
    '''
    Here comes the custom generation of the partial system states
    '''
    result = set()

    result.add(change_with_backwards_compatibility(comps, S1, W_NESTED))
    result.add(change_without_backwards_compatibility(comps, S2, W_BB))
    result.add(change_without_backwards_compatibility(comps, S3, W_UNIT_CHANGE))
    result.add(change_without_backwards_compatibility(comps, S4, W_CS_VARIABLE))
    
    return result


def change_with_backwards_compatibility(comps, system_state, partial_state):
    '''
    Models changes which are backwards compatible
    '''
    # Only fusion and nested must be in this state, for general the other is okay
    basic_and_conditions = [comps[FUSION].partial_state_by_name(partial_state), comps[PLAN].partial_state_by_name(partial_state)]
    basic_condition = PartialLogicalAndState("", basic_and_conditions, NO_TIME_FRAMES)
    
    # The conditions for a clean state is that all are in a nested state
    cleanup_and_conditions = []
    for cname in BASE_COMPS:
        cleanup_and_conditions.append(comps[cname].partial_state_by_name(partial_state))
    for cname in COMP_COMPS:
        # The compatibility component should not be in a partial compatible state
        cleanup_and_conditions.append(comps[cname].partial_state_by_name(partial_state.replace("w_", "wo_")))
    cleanup_condition = PartialLogicalAndState("", cleanup_and_conditions, NO_TIME_FRAMES)
    
    # Now create the change state
    return PartialSystemStateWithCleanup(system_state, *SS_TIMES[system_state], [basic_condition, cleanup_condition], cleanup_condition, NO_TIME_FRAMES)


def change_without_backwards_compatibility(comps, system_state, partial_state):
    '''
    Models changes which are not backwards compatible
    '''
    # In this case we either need a clean state for each component or we need the adapter to be in that state
    basic_and_conditions = []
    for cname in BASE_COMPS:
        or_condition = PartialLogicalOrState("", [comps[cname].partial_state_by_name(partial_state), comps[COMP_COMP].partial_state_by_name(partial_state)], NO_TIME_FRAMES)
        basic_and_conditions.append(or_condition)
    basic_condition = PartialLogicalAndState("", basic_and_conditions, NO_TIME_FRAMES)
    
    # The conditions for a clean state is that all are in a state with a variable
    cleanup_and_conditions = []
    for cname in BASE_COMPS:
        cleanup_and_conditions.append(comps[cname].partial_state_by_name(partial_state))
    for cname in COMP_COMPS:
        # The compatibility component should not be in a partial compatible state
        cleanup_and_conditions.append(comps[cname].partial_state_by_name(partial_state.replace("w_", "wo_")))
    cleanup_condition = PartialLogicalAndState("", cleanup_and_conditions, NO_TIME_FRAMES)
    
    # Now create the change state
    return PartialSystemStateWithCleanup(system_state, *SS_TIMES[system_state], [basic_condition, cleanup_condition], cleanup_condition, NO_TIME_FRAMES)
      
def generate_system_states(comps):
    '''
    Generates the system states from the variables given above
    '''
    
    # Setup the initial system state
    result = set()
    initial_partial_states = []
    for c in comps.values():
        initial_partial_states.extend(c.initial_states)
    initial_state_condition = PartialLogicalAndState("", initial_partial_states, NO_TIME_FRAMES)
    result.add(PartialSystemState(IS, -2, -2, -1, [initial_state_condition], NO_TIME_FRAMES))
    
    # No call the customize function
    for pss in customize(comps):
        result.add(pss)
        
    return result
        
def get_partial_state_availability(partial_state_name):
    '''
    Gets all partial states available foreach component
    '''
    # Get all the system states, this partial state belongs to
    system_states = []
    
    for k, v in SYSTEM_STATE_PARTIAL_STATE_MATCH.items():
        if partial_state_name in v:
            system_states.append(k)
            
    # If there is no associated system state, it is available from the beginning
    if len(system_states) == 0:
        return 0
    
    # Get the earliest dates the necessity of this state is known
    result = NO_TIME_FRAMES 
    for k in system_states:
        result = min(result, SS_TIMES_AVAILABLE[k])
    
    return result
        
            

def generate_components():
    '''
    Generates the components from the above defined variables
    '''
    comps = {}
    
    for c in ALL_COMPS:
        comp = Component(c, next_update_cycle[c][0], next_update_cycle[c][1], NO_TIME_FRAMES)
        # Now generate the partial state variables
        if c in BASE_COMPS:
            for ss in BASE_COMP_STATES:
                # We expect tuples of incompatible states
                assert len(ss) == 2
                for s in ss:
                    if s in INITIAL_STATES:
                        comp.add_initial_state(s, get_partial_state_availability(s))
                    else:
                        comp.add_partial_state(s, get_partial_state_availability(s))
                # Make them both incompatible
                comp.set_incompatibe(comp.partial_state_by_name(ss[0]), comp.partial_state_by_name(ss[1]))
        elif c in COMP_COMPS:
            for ss in COMP_COMP_STATES:
                # We expect tuples of incompatible states
                assert len(ss) == 2
                for s in ss:
                    if s in INITIAL_STATES:
                        comp.add_initial_state(s, get_partial_state_availability(s))
                    else:
                        comp.add_partial_state(s, get_partial_state_availability(s))
                # Make them both incompatible
                comp.set_incompatibe(comp.partial_state_by_name(ss[0]), comp.partial_state_by_name(ss[1]))
                
        # Add the new component to the components array
        comps[c] = comp
                
    return comps
    
       
def enforce_one_state_active(system_states, model):
    '''
    Enforces the global constraint, that one partial system state must be active
    per time frame
    '''
    for t in range(NO_TIME_FRAMES):
        pss = [ss.model_variable_for_time(t) for ss in system_states]
        model.Add(sum(pss) >= 1)
        
        
def print_exclude(ps_name, t):
    # Print helper
    return ps_name.startswith("wo")
        
        
def print_status(status):
    # Print helper
    status_str = ""
    
    if status == cp_model.OPTIMAL:
        status_str = "OPTIMAL"
    elif status == cp_model.FEASIBLE:
        status_str = "FEASIBLE"
    elif status == cp_model.INFEASIBLE:
        status_str = "INFEASIBLE"
    elif status == cp_model.MODEL_INVALID:
        status_str = "MODEL_INVALID"
    else:
        status_str = "UNKNOWN"
    print("Problem solved with status: %s" %status_str)
    
def print_objective(objective, solver):
    # Print helper
    print("Objective value is: %i" % (solver.Value(objective)))
  
def main():  
    # The main function, running all the stuff from above
    
    '''
    Build up the system states 
    '''
    comps = generate_components()
    
    # Set of all changes, which are successively added by the different change
    # requests
    system_states = generate_system_states(comps)
    
    '''
    Build the model
    '''
    # Creates the model to solve
    model = cp_model.CpModel()
    
    # Add the variables and directly set the in-between-constraints
    for c in comps.values():
        c.generate_model_variables(model)
    
    # Add the variables for the partial system states
    for ss in system_states:
        ss.generate_model_variables(model)
    
    # Set the internal compatibility constraints
    for c in comps.values():
        c.add_constraints(model)
        
    # Set the system state active constraints
    for ss in system_states:
        ss.add_constraints(model)
        
    # Make sure at least one partial system state is active at a time
    enforce_one_state_active(system_states, model)
    
    
    '''
    Set the objective function
    '''
    solver = cp_model.CpSolver()
    
    # The overall objective value
    objective_function = model.NewIntVar(0, len(comps)*(NO_TIME_FRAMES-1), 'objective_function')
    # Get all the single change objective function parts and add them
    of_parts = [c.get_few_changes_objective(model) for c in comps.values()]
    # Connect the objective_function with the of_parts
    model.Add(sum(of_parts) == objective_function)
    
    # Set the objective function
    model.Minimize(objective_function)
    
    '''
    Solve the model and print the results
    '''
    # Instead of just solving a single model with status = solver.Solve(model) we try to find all solutions
    status = solver.Solve(model)
    print_status(status)
    print_objective(objective_function, solver)
    print_comps_result(NO_TIME_FRAMES, comps, solver, print_exclude)
    print_system_state_result(NO_TIME_FRAMES, system_states, solver)
    
    
if __name__ == '__main__':
    main()
    
    